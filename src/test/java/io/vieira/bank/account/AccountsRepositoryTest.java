package io.vieira.bank.account;

import io.vieira.bank.Bank;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountsRepositoryTest {

    @ParameterizedTest
    @MethodSource("deposits")
    public void shouldBeAbleToDepositToAccount(BigDecimal initialAmount, BigDecimal toAdd, BigDecimal expected) {
        TestAccountsRepository accountsRepository = new TestAccountsRepository(new Account(100, initialAmount));
        Bank bank = new Bank(new InMemoryAccountManager(null, accountsRepository, null));
        bank.deposit(100, toAdd);

        assertThat(accountsRepository.consult(100)).isEqualTo(expected);
    }

    @ParameterizedTest
    @MethodSource("deposits")
    public void shouldNotBeAbleToDepositNegativeAmounts(BigDecimal initialAmount, BigDecimal toAdd) {
        TestAccountsRepository accountsRepository = new TestAccountsRepository(new Account(100, initialAmount));
        Bank bank = new Bank(new InMemoryAccountManager(null, accountsRepository, null));

        assertThatThrownBy(() -> bank.deposit(100, toAdd.negate()))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("must be positive");
    }

    private static Stream<Arguments> deposits(){
        return IntStream
                .range(5000, 6000)
                .mapToObj(value -> {
                    BigDecimal initialAmount = new BigDecimal(value);
                    BigDecimal toDeposit = initialAmount.divideToIntegralValue(BigDecimal.TEN);
                    BigDecimal expected = initialAmount.add(toDeposit);
                    return Arguments.of(initialAmount, toDeposit, expected);
                });
    }

    @ParameterizedTest
    @MethodSource("withdrawals")
    public void shouldBeAbleToWithdrawFromAccount(BigDecimal initialAmount, BigDecimal toWithdraw, BigDecimal expected) {
        TestAccountsRepository accountsRepository = new TestAccountsRepository(new Account(100, initialAmount));
        Bank bank = new Bank(new InMemoryAccountManager(null, accountsRepository, null));
        bank.withdraw(100, toWithdraw);

        assertThat(accountsRepository.consult(100)).isEqualTo(expected);
    }

    @ParameterizedTest
    @MethodSource("withdrawals")
    public void shouldNotBeAbleToWithdrawNegativeAmounts(BigDecimal initialAmount, BigDecimal toWithdraw) {
        TestAccountsRepository accountsRepository = new TestAccountsRepository(new Account(100, initialAmount));
        Bank bank = new Bank(new InMemoryAccountManager(null, accountsRepository, null));

        assertThatThrownBy(() -> bank.withdraw(100, toWithdraw.negate()))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("must be positive");
    }

    private static Stream<Arguments> withdrawals() {
        return IntStream.range(15000, 16000).mapToObj(value -> {
            BigDecimal initialAmount = new BigDecimal(value);
            BigDecimal toWithdraw = initialAmount.divideToIntegralValue(BigDecimal.TEN);
            BigDecimal expected = initialAmount.subtract(toWithdraw);
            return Arguments.of(initialAmount, toWithdraw, expected);
        });
    }


    @ParameterizedTest
    @MethodSource("invalidWithdrawals")
    public void shouldDenyWithdrawalsWhenThereIsNotEnoughBalance(BigDecimal initialAmount, BigDecimal toWithdraw) {
        TestAccountsRepository accountsRepository = new TestAccountsRepository(new Account(100, initialAmount));
        Bank bank = new Bank(new InMemoryAccountManager(null, accountsRepository, null));

        assertThatThrownBy(() -> bank.withdraw(100, toWithdraw)).isInstanceOf(NotEnoughFundsException.class);
    }

    private static Stream<Arguments> invalidWithdrawals() {
        return IntStream.range(15000, 16000).mapToObj(value -> {
            BigDecimal initialAmount = new BigDecimal(value);
            return Arguments.of(initialAmount, initialAmount.multiply(BigDecimal.TEN));
        });
    }

    @ParameterizedTest
    @MethodSource("transfers")
    public void shouldBeAbleToDoTransfersBetweenAccounts(BigDecimal initialAmount, BigDecimal toTransfer, BigDecimal expectedOnSource, BigDecimal expectedOnDestination) {
        TestAccountsRepository accountsRepository = new TestAccountsRepository(
                new Account(100, initialAmount),
                new Account(101, initialAmount)
        );
        Bank bank = new Bank(new InMemoryAccountManager(null, accountsRepository, null));
        bank.transfer(100, 101, toTransfer);

        assertThat(accountsRepository.consult(100)).isEqualTo(expectedOnSource);
        assertThat(accountsRepository.consult(101)).isEqualTo(expectedOnDestination);
    }

    private static Stream<Arguments> transfers() {
        return IntStream.range(15000, 16000).mapToObj(value -> {
            BigDecimal initialAmount = new BigDecimal(value);
            BigDecimal toTransfer = initialAmount.divideToIntegralValue(BigDecimal.TEN);
            BigDecimal expectedOnSource = initialAmount.subtract(toTransfer);
            BigDecimal expectedOnDestination = initialAmount.add(toTransfer);
            return Arguments.of(initialAmount, toTransfer, expectedOnSource, expectedOnDestination);
        });
    }

}
