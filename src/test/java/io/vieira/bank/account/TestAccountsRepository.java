package io.vieira.bank.account;

import java.math.BigDecimal;

public class TestAccountsRepository extends AccountsRepository {

    public TestAccountsRepository(Account... accounts) {
        super(accounts);
    }

    public BigDecimal consult(Integer accountNumber) {
        return this.accounts.get(accountNumber);
    }
}
