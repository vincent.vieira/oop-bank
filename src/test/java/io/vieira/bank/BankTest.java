package io.vieira.bank;

import io.vieira.bank.account.Account;
import io.vieira.bank.account.AccountManager;
import io.vieira.bank.account.AccountsRepository;
import io.vieira.bank.account.InMemoryAccountManager;
import io.vieira.bank.audit.Operation;
import io.vieira.bank.audit.OperationFilter;
import io.vieira.bank.audit.TestOperationsLogger;
import io.vieira.bank.audit.TestOperationsPrinter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static io.vieira.bank.audit.OperationFilters.*;
import static org.assertj.core.api.Assertions.assertThat;

public class BankTest {

    private TestOperationsPrinter operationsPrinter = new TestOperationsPrinter();

    private TestOperationsLogger operationsLogger = new TestOperationsLogger();

    @Test
    public void shouldLogAllOperationsOnAnAccount() {
        AccountManager accountManager = new InMemoryAccountManager(
                this.operationsLogger,
                new AccountsRepository(new Account(100, new BigDecimal(100000))),
                operationsPrinter
        );
        Bank bank = new Bank(accountManager);
        triggerSequentialOperations(bank);

        assertThat(operationsLogger.totalCount(100)).isEqualTo(1000);
        assertThat(operationsLogger.withdrawalsCount(100)).isEqualTo(500);
        assertThat(operationsLogger.depositsCount(100)).isEqualTo(500);
    }

    @Test
    public void shouldPrintAllOperationsOnAnAccount() {
        AccountManager accountManager = new InMemoryAccountManager(
                this.operationsLogger,
                new AccountsRepository(new Account(100, new BigDecimal(100000))),
                operationsPrinter
        );
        Bank bank = new Bank(accountManager);
        triggerSequentialOperations(bank);

        bank.printReport(100);

        assertThat(operationsPrinter.totalCount()).isEqualTo(1000);
        assertThat(operationsPrinter.withdrawalsCount()).isEqualTo(500);
        assertThat(operationsPrinter.depositsCount()).isEqualTo(500);
    }

    private void triggerSequentialOperations(Bank bank) {
        IntStream
                .rangeClosed(500, 1500)
                .forEach(value -> {
                    switch(value % 2) {
                        case 0:
                            bank.deposit(100, new BigDecimal(value));
                            break;
                        default:
                            bank.withdraw(100, new BigDecimal(value));
                            break;
                    }
                });
    }

    @ParameterizedTest
    @MethodSource("operations")
    public void shouldAllowFilteringOnReportBeforePrintingIt(List<Operation> operations, OperationFilter filter, Integer expectedPrints) {
        this.operationsLogger.seed(100, operations);

        AccountManager accountManager = new InMemoryAccountManager(
                this.operationsLogger,
                new AccountsRepository(new Account(100, new BigDecimal(100000))),
                operationsPrinter
        );
        Bank bank = new Bank(accountManager);
        bank.printReport(100, filter);

        assertThat(operationsPrinter.totalCount()).isEqualTo(expectedPrints);
    }

    private static Stream<Arguments> operations() {
        List<Operation> operations = Arrays.asList(
                new Operation(UUID.randomUUID(), new BigDecimal(100), new BigDecimal(100000), LocalDateTime.of(2017, 1, 30, 12, 30)),
                new Operation(UUID.randomUUID(), new BigDecimal(-300), new BigDecimal(100100), LocalDateTime.of(2017, 2, 1, 12, 30)),
                new Operation(UUID.randomUUID(), new BigDecimal(500), new BigDecimal(99800), LocalDateTime.of(2017, 2, 2, 12, 30))
        );
        return Stream.of(
                Arguments.of(operations, after(LocalDateTime.of(2017, 2, 1, 11, 0)), 2),
                Arguments.of(operations, before(LocalDateTime.of(2017, 2, 1, 11, 0)), 1),
                Arguments.of(operations, between(
                        LocalDateTime.of(2017, 2, 1, 11, 0),
                        LocalDateTime.of(2017, 2, 2, 11, 0)
                ), 1),
                Arguments.of(operations, lessThan(new BigDecimal(200)), 2),
                Arguments.of(operations, greaterThan(new BigDecimal(200)), 1),
                Arguments.of(operations, between(new BigDecimal(50), new BigDecimal(600)), 2),
                Arguments.of(
                        operations,
                        after(LocalDateTime.of(2017, 2, 1, 11, 0)).and(lessThan(BigDecimal.ZERO)),
                        1
                )
        );
    }
}
