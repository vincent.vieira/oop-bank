package io.vieira.bank.audit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TestOperationsLogger extends InMemoryOperationsLogger {

    public void seed(Integer accountNumber, List<Operation> operations) {
        this.operations.compute(accountNumber, (number, initialOperations) -> operations);
    }

    public int withdrawalsCount(int accountNumber) {
        return (int) operations
                .getOrDefault(accountNumber, new ArrayList<>())
                .stream()
                .filter(operation -> operation.amount.compareTo(BigDecimal.ZERO) < 0)
                .count();
    }

    public int totalCount(int accountNumber) {
        return operations
                .getOrDefault(accountNumber, new ArrayList<>())
                .size();
    }

    public int depositsCount(int accountNumber) {
        return (int) operations
                .getOrDefault(accountNumber, new ArrayList<>())
                .stream()
                .filter(operation -> operation.amount.compareTo(BigDecimal.ZERO) > 0)
                .count();
    }
}
