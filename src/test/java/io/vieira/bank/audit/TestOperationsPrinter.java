package io.vieira.bank.audit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestOperationsPrinter implements OperationsPrinter {

    private final Map<Integer, List<Operation>> operations = new HashMap<>();

    @Override
    public void print(Integer accountNumber, Operation operation) {
        int operationAmountNegativeOrPositive = operation.amount.compareTo(BigDecimal.ZERO);
        operations.computeIfAbsent(operationAmountNegativeOrPositive, amountType -> new ArrayList<>());
        operations.computeIfPresent(operationAmountNegativeOrPositive, (integer, operations) -> {
            operations.add(operation);
            return operations;
        });
    }

    public int withdrawalsCount() {
        return operations
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey() < 0)
                .map(Map.Entry::getValue)
                .mapToInt(List::size)
                .sum();
    }

    public int totalCount() {
        return operations
                .values()
                .stream()
                .mapToInt(List::size)
                .sum();
    }

    public int depositsCount() {
        return operations
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey() > 0)
                .map(Map.Entry::getValue)
                .mapToInt(List::size)
                .sum();
    }
}
