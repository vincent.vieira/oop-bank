package io.vieira.bank;

import io.vieira.bank.account.AccountManager;
import io.vieira.bank.audit.OperationFilter;

import java.math.BigDecimal;

public class Bank {

    private final AccountManager accountManager;

    public Bank(AccountManager accountManager){
        this.accountManager = accountManager;
    }

    public void deposit(Integer targetAccount, BigDecimal amount) {
        accountManager.deposit(targetAccount, amount);
    }

    public void transfer(Integer sourceAccount, Integer destinationAccount, BigDecimal transferAmount) {
        accountManager.transfer(sourceAccount, destinationAccount, transferAmount);
    }

    public void withdraw(Integer targetAccount, BigDecimal amount) {
        accountManager.withdraw(targetAccount, amount);
    }

    public void printReport(Integer accountNumber) {
        accountManager.printReport(accountNumber);
    }

    public void printReport(Integer accountNumber, OperationFilter filter) {
        accountManager.printReport(accountNumber, filter);
    }
}
