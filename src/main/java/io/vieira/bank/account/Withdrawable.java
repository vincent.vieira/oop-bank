package io.vieira.bank.account;

import java.math.BigDecimal;

public interface Withdrawable {
    BigDecimal withdraw(Integer targetAccount, BigDecimal amount);
}
