package io.vieira.bank.account;

import java.math.BigDecimal;

public class Account {

    final Integer number;

    final BigDecimal balance;

    public Account(Integer number, BigDecimal initialBalance) {
        this.number = number;
        this.balance = initialBalance;
    }
}
