package io.vieira.bank.account;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class AccountsRepository implements Withdrawable, Depositable {

    final Map<Integer, BigDecimal> accounts;

    public AccountsRepository(Account... accounts) {
        this.accounts = Arrays
                .stream(accounts)
                .collect(Collectors.toMap(account -> account.number, account -> account.balance));
    }

    @Override
    public BigDecimal deposit(Integer targetAccount, BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Amount must be positive");
        }

        return accounts.computeIfPresent(
                targetAccount,
                (accountNumber, currentAmount) -> currentAmount.add(amount)
        );
    }
    @Override
    public BigDecimal withdraw(Integer targetAccount, BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Amount must be positive");
        }

        BiFunction<Integer, BigDecimal, BigDecimal> processor = (accountNumber, currentAmount) -> {
            BigDecimal newBalance = currentAmount.subtract(amount);
            if (newBalance.compareTo(BigDecimal.ZERO) < 0) {
                throw new NotEnoughFundsException();
            }
            return newBalance;
        };

        return accounts.computeIfPresent(
                targetAccount,
                processor
        );
    }
}
