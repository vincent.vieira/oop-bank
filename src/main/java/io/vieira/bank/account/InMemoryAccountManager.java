package io.vieira.bank.account;

import io.vieira.bank.audit.Operation;
import io.vieira.bank.audit.OperationFilter;
import io.vieira.bank.audit.OperationsLogger;
import io.vieira.bank.audit.OperationsPrinter;

import java.math.BigDecimal;
import java.util.List;

public class InMemoryAccountManager implements AccountManager {

    private final AccountsRepository accountsRepository;

    private final OperationsLogger operationsLogger;

    private final OperationsPrinter operationsPrinter;

    public InMemoryAccountManager(OperationsLogger operationsLogger, AccountsRepository accountsRepository, OperationsPrinter operationsPrinter) {
        this.operationsLogger = operationsLogger;
        this.accountsRepository = accountsRepository;
        this.operationsPrinter = operationsPrinter;
    }

    @Override
    public void transfer(Integer sourceAccount, Integer destinationAccount, BigDecimal transferAmount) {
        BigDecimal newBalanceOnSourceAccount = accountsRepository.withdraw(sourceAccount, transferAmount);
        if (operationsLogger != null) {
            operationsLogger.log(sourceAccount, new Operation(transferAmount.negate(), newBalanceOnSourceAccount));
        }
        BigDecimal newBalanceOnDestinationAccount = accountsRepository.deposit(destinationAccount, transferAmount);
        if (operationsLogger != null) {
            operationsLogger.log(destinationAccount, new Operation(transferAmount, newBalanceOnDestinationAccount));
        }
    }

    @Override
    public void withdraw(Integer targetAccount, BigDecimal amount) {
        BigDecimal newBalance = accountsRepository.withdraw(targetAccount, amount);
        if (operationsLogger != null) {
            operationsLogger.log(targetAccount, new Operation(amount.negate(), newBalance));
        }
    }

    @Override
    public void deposit(Integer targetAccount, BigDecimal amount) {
        BigDecimal newBalance = accountsRepository.deposit(targetAccount, amount);
        if (operationsLogger != null) {
            operationsLogger.log(targetAccount, new Operation(amount, newBalance));
        }
    }

    @Override
    public void printReport(Integer accountNumber) {
        List<Operation> operations = operationsLogger.getOperationsFor(accountNumber, operation -> true);

        if (operationsPrinter != null) {
            operations.forEach(operation -> operationsPrinter.print(accountNumber, operation));
        }
    }

    @Override
    public void printReport(Integer accountNumber, OperationFilter filter) {
        List<Operation> operations = operationsLogger.getOperationsFor(accountNumber, filter);

        if (operationsPrinter != null) {
            operations.forEach(operation -> operationsPrinter.print(accountNumber, operation));
        }
    }
}
