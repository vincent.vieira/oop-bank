package io.vieira.bank.account;

import java.math.BigDecimal;

public interface Transferable {
    void transfer(Integer sourceAccount, Integer destinationAccount, BigDecimal transferAmount);
}
