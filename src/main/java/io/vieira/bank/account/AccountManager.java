package io.vieira.bank.account;

import io.vieira.bank.audit.Reportable;

import java.math.BigDecimal;

public interface AccountManager extends Transferable, Reportable {
    void deposit(Integer targetAccount, BigDecimal amount);
    void withdraw(Integer targetAccount, BigDecimal amount);
}
