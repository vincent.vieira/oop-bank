package io.vieira.bank.account;

import java.math.BigDecimal;

public interface Depositable {
    BigDecimal deposit(Integer targetAccount, BigDecimal amount);
}
