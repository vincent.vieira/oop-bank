package io.vieira.bank.audit;

import java.util.function.Predicate;

public interface OperationFilter extends Predicate<Operation> {

    default OperationFilter and(OperationFilter other) {
        return (operation) -> test(operation) && other.test(operation);
    }
}
