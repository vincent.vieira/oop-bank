package io.vieira.bank.audit;

public interface Reportable {
    void printReport(Integer accountNumber);
    void printReport(Integer accountNumber, OperationFilter filter);
}
