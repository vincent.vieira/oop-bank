package io.vieira.bank.audit;

import java.util.List;

public interface OperationsLogger {
    void log(Integer accountNumber, Operation operation);
    List<Operation> getOperationsFor(Integer accountNumber, OperationFilter filter);
}
