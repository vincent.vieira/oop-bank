package io.vieira.bank.audit;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class OperationFilters {

    public static OperationFilter after(LocalDateTime targetDateTime) {
        return operation -> operation.dateTime.isAfter(targetDateTime);
    }

    public static OperationFilter before(LocalDateTime targetDateTime) {
        return operation -> operation.dateTime.isBefore(targetDateTime);
    }

    public static OperationFilter between(LocalDateTime beginDateTime, LocalDateTime endDateTime) {
        return after(beginDateTime).and(before(endDateTime));
    }

    public static OperationFilter greaterThan(BigDecimal amount) {
        return operation -> operation.amount.compareTo(amount) > 0;
    }

    public static OperationFilter lessThan(BigDecimal amount) {
        return operation -> operation.amount.compareTo(amount) < 0;
    }

    public static OperationFilter between(BigDecimal startAmount, BigDecimal endAmount) {
        return greaterThan(startAmount).and(lessThan(endAmount));
    }
}
