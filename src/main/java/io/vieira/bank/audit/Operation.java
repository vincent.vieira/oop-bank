package io.vieira.bank.audit;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class Operation {

    final UUID id;

    final LocalDateTime dateTime;

    final BigDecimal amount;

    final BigDecimal balance;

    public Operation(UUID id, BigDecimal amount, BigDecimal balance, LocalDateTime executionDate) {
        this.id = id;
        this.dateTime = executionDate;
        this.amount = amount;
        this.balance = balance;
    }

    public Operation(BigDecimal amount, BigDecimal balance) {
        this(UUID.randomUUID(), amount, balance, LocalDateTime.now());
    }
}