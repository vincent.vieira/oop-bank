package io.vieira.bank.audit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemoryOperationsLogger implements OperationsLogger {

    final Map<Integer, List<Operation>> operations = new HashMap<>();

    @Override
    public void log(Integer accountNumber, Operation operation) {
        operations.computeIfPresent(
                accountNumber,
                (foundAccountNumber, operationsForAccount) -> {
                    operationsForAccount.add(operation);
                    return operationsForAccount;
                }
        );
        operations.putIfAbsent(accountNumber, new ArrayList<>());
    }

    @Override
    public List<Operation> getOperationsFor(Integer accountNumber, OperationFilter filter) {
        return operations.getOrDefault(accountNumber, new ArrayList<>())
                .stream()
                .filter(filter)
                .collect(Collectors.toList());
    }
}
