package io.vieira.bank.audit;

public interface OperationsPrinter {
    void print(Integer accountNumber, Operation operation);
}
