package io.vieira.bank.audit;

import java.time.format.DateTimeFormatter;

public class ConsoleOperationsPrinter implements OperationsPrinter {

    @Override
    public void print(Integer accountNumber, Operation operation) {
        System.out.println(String.format(
                "Operation #%s at %s on account number #%d: balance is %s after operation of amount %s",
                operation.id,
                operation.dateTime.format(DateTimeFormatter.ISO_DATE),
                accountNumber,
                operation.balance,
                operation.amount
        ));
    }
}
